from django.db.models import Q
from django.views.generic import ListView, DetailView

from blog.models import Post, Category


class PostsListView(ListView):
    model = Post
    paginate_by = 30
    queryset = Post.active_objects.all()
    category = None

    def get_queryset(self):
        category_slug = self.kwargs.get('slug')
        q = self.request.GET.get('q')
        queryset = super().get_queryset()
        if category_slug is not None:
            self.category = Category.objects.get(slug=category_slug)
            queryset = queryset.filter(categories__slug=category_slug)
        if q is not None:
            queryset = queryset.filter(Q(header__icontains=q) | Q(annotation__icontains=q) | Q(content__icontains=q))
        return queryset

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data()
        context['title'] = self.category
        return context


class PostDetailView(DetailView):
    model = Post

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['title'] = self.object.header
        return context
