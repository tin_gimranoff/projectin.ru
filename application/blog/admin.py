from django.contrib import admin

from blog.models import Category, Post


class PostAdmin(admin.ModelAdmin):
    list_display = ('header', 'publish_date', 'isDraft')


admin.site.register(Category)
admin.site.register(Post, PostAdmin)
