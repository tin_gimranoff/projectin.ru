from datetime import datetime
from django.db import models
from django_resized import ResizedImageField
from cked.fields import RichTextField
from pytils.translit import slugify


class Category(models.Model):
    class Meta:
        db_table = 'categories'
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'
        ordering = ('name',)

    name = models.CharField(verbose_name='Название категории', max_length=255)
    slug = models.SlugField(verbose_name='Алиас', unique=True, blank=True, null=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super().save(*args, **kwargs)


class ActivePosts(models.Manager):
    def get_queryset(self):
        now = datetime.now()
        return super().get_queryset().filter(publish_date__lte=now, isDraft=False)


class Post(models.Model):
    objects = models.Manager()
    active_objects = ActivePosts()

    class Meta:
        db_table = 'posts'
        verbose_name = 'Пост'
        verbose_name_plural = 'Посты'
        ordering = ('-publish_date',)

    header = models.CharField(verbose_name='Заголовок', max_length=255)
    slug = models.SlugField(verbose_name='Алиас', max_length=255, blank=True, null=True)
    annotation = models.CharField(verbose_name='Аннотация', max_length=255)
    image = ResizedImageField(verbose_name='Изображение', size=[500, 340], crop=['middle', 'center'], quality=100,
                              upload_to='posts')
    publish_date = models.DateTimeField(verbose_name='Дата публикации')
    categories = models.ManyToManyField('Category', verbose_name='Категории')
    content = RichTextField(verbose_name='Контент')
    isDraft = models.BooleanField(verbose_name='Черновик', default=True)

    def __str__(self):
        return self.header

    def save(self, *args, **kwargs):
        print(1)
        self.slug = slugify(self.header)
        super().save(*args, **kwargs)

