from django.template.defaultfilters import register


@register.filter
def times(count):
    return range(1, int(count)+1)
