from django import template

from blog.models import Category, Post

register = template.Library()


@register.inclusion_tag('sidebar/last_posts.html')
def last_posts():
    return {
        'posts': Post.active_objects.all()[0:3]
    }
