from django import template
from django.utils.safestring import mark_safe

register = template.Library()


@register.simple_tag(takes_context=True)
def show_title_in_header(context):
    return mark_safe(f"{context.get('title')} —&nbsp;" if context.get('title') else "")
