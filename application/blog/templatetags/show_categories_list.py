from django import template

from blog.models import Category

register = template.Library()


@register.inclusion_tag('sidebar/categories.html', takes_context=True)
def show_categories_list(context):
    return {
        'categories': Category.objects.all(),
        'url': context['request'].path_info
    }
