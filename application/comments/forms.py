from django import forms
from django_comments.forms import CommentForm


class CustomCommentForm(CommentForm):
    name = forms.CharField(max_length=50, required=True, widget=forms.TextInput(
        attrs={
            'placeholder': 'Имя',
            'maxlength': 50,
        }
    ))
    email = forms.EmailField(max_length=50, required=True, widget=forms.EmailInput(
        attrs={
            'placeholder': 'E-mail',
            'maxlength': 50,
        }
    ))
    comment = forms.CharField(max_length=3000, required=True, widget=forms.Textarea(
        attrs={
            'placeholder': 'Введите ваш комментарий',
            'rows': 3,
            'cols': 40,
            'maxlength': 3000,
        },
    ))
